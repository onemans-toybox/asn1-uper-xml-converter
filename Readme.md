## 環境設定

https://github.com/vlm/asn1c

9925dbbda86b436896108439ea3e0a31280a6065


```bash
git clone https://github.com/vlm/asn1c.git
cd asn1c
test -f configure || autoreconf -iv
./configure
sudo make install
cd ..
mkdir src/generated
mkdir lib
cd src/generated
asn1c data/rocket.asn -D . -gen-autotools -fcompound-names
autoreconf --force --install
autoconf -i
./configure --prefix=/path/to/lib
make install
cd ..
make
./main.out
```