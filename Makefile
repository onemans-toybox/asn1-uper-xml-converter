CCFLAGS=-g
CFLAGS=$(CCFLAGS)

main.out:
	$(CC) $(CFLAGS) -I ./src/generated/ -o main.out src/main.c lib/lib/libasncodec.a

clean:
	+rm main.out